<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ChatMessagesReading
 *
 * @property int $id
 * @property int $chat_id
 * @property int $user_id
 * @property int|null $max_read_msg_id
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessagesReading newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessagesReading newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessagesReading query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessagesReading whereChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessagesReading whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessagesReading whereMaxReadMsgId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessagesReading whereUserId($value)
 * @mixin \Eloquent
 */
class ChatMessagesReading extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'chat_id',
        'user_id',
        'max_read_msg_id'
    ];
}
