<?php

namespace App\Models;

use App\Enums\ChatTypesEnum;

/**
 * App\Models\Dialogue
 *
 * @property int $id
 * @property string|null $name
 * @property string $chat_type
 * @property int|null $creator_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ChatMessage|null $latestMessage
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ChatMessage> $messages
 * @property-read int|null $messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ChatMessage> $newMessages
 * @property-read int|null $new_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ChatParticipant> $participants
 * @property-read int|null $participants_count
 * @property-read \App\Models\ChatParticipant|null $recipient
 * @method static \Illuminate\Database\Eloquent\Builder|Dialogue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Dialogue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Dialogue query()
 * @method static \Illuminate\Database\Eloquent\Builder|Dialogue whereChatType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dialogue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dialogue whereCreatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dialogue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dialogue whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dialogue whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Dialogue extends Chat
{
    protected static $singleTableType = 'dialogue';
}
