<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;
use Staudenmeir\EloquentEagerLimit\HasEagerLimit;

/**
 * App\Models\Chat
 *
 * @property int $id
 * @property string|null $name
 * @property string $chat_type
 * @property int|null $creator_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ChatMessage|null $latestMessage
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ChatMessage> $messages
 * @property-read int|null $messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ChatMessage> $newMessages
 * @property-read int|null $new_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ChatParticipant> $participants
 * @property-read int|null $participants_count
 * @property-read \App\Models\ChatParticipant|null $recipient
 * @method static \Illuminate\Database\Eloquent\Builder|Chat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Chat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Chat query()
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereChatType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereCreatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Chat extends Model
{
    use HasFactory, HasEagerLimit, SingleTableInheritanceTrait;

    protected $table = 'chats';
    protected static string $singleTableTypeField = 'chat_type';

    protected static array $singleTableSubclasses = [Dialogue::class];

    /**
     * @return HasMany
     */
    public function messages(): HasMany
    {
        return $this->hasMany(ChatMessage::class, 'chat_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function participants(): HasMany
    {
        return $this->hasMany(ChatParticipant::class, 'chat_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function recipient(): HasOne
    {
        return $this->participants()->one()
            ->where('chat_participants.participant_id', '!=', auth()->id());
    }

    /**
     * @return HasOne
     */
    public function latestMessage(): HasOne
    {
        return $this->messages()->one()->latest();
    }

    /**
     * @return HasMany
     */
    public function newMessages(): HasMany
    {
        return $this->messages()
            ->where('author_id', '!=', auth()->id())
            ->where('chat_messages.id', '>',
                DB::raw('(SELECT max_read_msg_id from chat_messages_readings
                    WHERE chat_id = chats.id AND user_id = '.auth()->id().')'));
    }

}
