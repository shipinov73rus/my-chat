<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use Staudenmeir\EloquentEagerLimit\HasEagerLimit;

/**
 * App\Models\ChatMessage
 *
 * @property int $id
 * @property string $message
 * @property int $chat_id
 * @property int $author_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $author
 * @property-read \App\Models\Chat|null $chat
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ChatMessagesReading> $readings
 * @property-read int|null $readings_count
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereUpdatedAt($value)
 * @method static Builder|ChatMessage withIsMessageRead(string $alias = 'is_read')
 * @mixin \Eloquent
 */
class ChatMessage extends Model
{
    use HasFactory, HasEagerLimit;

    protected $fillable = [
        'message',
        'chat_id',
        'author_id',
    ];

    /**
     * @return BelongsTo
     */
    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class, 'id', 'chat_id');
    }

    /**
     * @return BelongsTo
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function readings(): HasMany
    {
        return $this->hasMany(ChatMessagesReading::class, 'chat_id', 'chat_id');
    }

    /**
     * @param  Builder  $builder
     * @param  string  $alias
     *
     * @return Builder
     */
    public function scopeWithIsMessageRead(Builder $builder, string $alias = 'is_read'): Builder
    {
        return $builder->withExists(['readings as '.$alias => function ($builder) {
            $builder->where('user_id', '!=', DB::raw('chat_messages.author_id'))
                ->where('max_read_msg_id', '>=', DB::raw('chat_messages.id'));
        }]);
    }

}
