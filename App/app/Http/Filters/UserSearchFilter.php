<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class UserSearchFilter implements Filter
{

    /**
     * @param  Builder  $query
     * @param $value
     * @param  string  $property
     *
     * @return Builder
     */
    public function __invoke(Builder $query, $value, string $property): Builder
    {
        $query->where(function ($query) use ($value) {
            $query->where('name', 'iLIKE', "%$value%")->orWhere('last_name', 'iLIKE', "%$value%");
        });

        return $query;
    }
}
