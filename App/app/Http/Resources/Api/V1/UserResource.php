<?php

namespace App\Http\Resources\Api\V1;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Tymon\JWTAuth\Factory;
use Tymon\JWTAuth\JWTGuard;

class UserResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array (int|null|string)[]
     *
     */
    public function toArray($request): array
    {
        /** @var User $resource */
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'last_name' => $resource->last_name,
            'created_at' => $resource->created_at,
            'new_messages_chats' => $this->whenCounted('chats')
        ];
    }
}
