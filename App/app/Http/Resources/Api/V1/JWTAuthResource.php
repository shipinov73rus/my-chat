<?php

namespace App\Http\Resources\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Tymon\JWTAuth\Factory;
use Tymon\JWTAuth\JWTGuard;

class JWTAuthResource extends JsonResource
{
    /**
     * @param $resource
     * @param  string  $token
     */
    public function __construct($resource, private readonly string $token)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array (int|null|string)[]
     *
     */
    public function toArray($request): array
    {
        /** @var Factory $factory */
        $factory = auth()->factory();

        return [
            'user_id' => $this->resource->id,
            'expires_in' => $factory->getTTL() * 60,
            'access_token' => $this->token
        ];
    }
}
