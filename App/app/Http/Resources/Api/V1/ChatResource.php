<?php

namespace App\Http\Resources\Api\V1;

use App\Models\Chat;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ChatResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array (int|null|string)[]
     *
     */
    public function toArray($request): array
    {
        /** @var Chat $resource */
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'last_message' => new MessageResource($this->whenLoaded('latestMessage')),
            'new_messages' => $resource->new_messages_count,
            'recipient' => new UserResource($resource->recipient->user),
        ];
    }
}
