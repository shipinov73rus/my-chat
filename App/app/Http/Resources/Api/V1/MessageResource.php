<?php

namespace App\Http\Resources\Api\V1;

use App\Models\Chat;
use App\Models\ChatMessage;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array (int|null|string)[]
     *
     */
    public function toArray($request): array
    {
        /** @var ChatMessage $resource */
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'message' => $resource->message,
            'is_read' => $resource->is_read,
            'author' => new UserResource($resource->author),
            'created_at' => $resource->created_at
        ];
    }
}
