<?php

namespace App\Http\Requests\Api\V1;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'login' => ['required', 'max:32', 'unique:users,login', 'min:5'],
            'password' => ['required', 'min:8', 'confirmed', 'max:255'],
            'name' => ['required', 'max:255', 'min:3'],
            'last_name' => ['required', 'max:255', 'min:3'],
        ];
    }
}
