<?php

namespace App\Http\Controllers\Api\V1;

use App\Contracts\ChatServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\SendChatMessageRequest;
use App\Http\Resources\Api\V1\ChatResource;
use App\Http\Resources\Api\V1\MessageResource;
use App\Http\Resources\Api\V1\UserResource;
use App\Models\Chat;
use Illuminate\Http\JsonResponse;

class ChatController extends Controller
{

    public function __construct(private readonly ChatServiceContract $service)
    {
        parent::__construct(request());
    }
    /**
     * @return array
     */
    public function getChatsList(): array
    {
        $dialogues = $this->service->getDialoguesListBuilder();

        return $this->paginateQuerySet($dialogues, ChatResource::class);
    }

    /**
     * @param  SendChatMessageRequest  $request
     *
     * @return JsonResponse
     */
    public function sendMessage(SendChatMessageRequest $request): JsonResponse
    {
        $data = $request->validated();
        $message = $this->service->sendMessageAction($data);

        return response()->json($message, 201);
    }

    /**
     * @param  Chat  $chat
     *
     * @return array
     */
    public function getChatMessages(Chat $chat): array
    {
        $messages = $this->service->getChatMessagesBuilder($chat);
        $recipient = $this->service->getChatRecipient($chat);

        return $this->cursorPaginatedQuerySet($messages, MessageResource::class,
            ['recipient' => new UserResource($recipient->user)]
        );
    }
}
