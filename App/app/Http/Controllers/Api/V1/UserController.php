<?php

namespace App\Http\Controllers\Api\V1;

use App\Contracts\UserServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\V1\UserResource;
use App\Models\User;

class UserController extends Controller
{

    public function __construct(private readonly UserServiceContract $service) {
        parent::__construct(request());
    }

    /**
     * @return array
     */
    public function getUsersList(): array
    {
        $users = $this->service->getUsersListBuilder();

        return $this->paginateQuerySet($users, UserResource::class);
    }

    /**
     * @return UserResource
     */
    public function getMe(): UserResource
    {
        $me = $this->service->getMe();

        return new UserResource($me);
    }

    /**
     * @param  User  $user
     *
     * @return UserResource
     */
    public function getUser(User $user): UserResource
    {
        return new UserResource($user);
    }
}
