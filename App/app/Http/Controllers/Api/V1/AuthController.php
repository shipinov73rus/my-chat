<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\LoginRequest;
use App\Http\Requests\Api\V1\RegistrationRequest;
use App\Http\Resources\Api\V1\JWTAuthResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\JWTGuard;

class AuthController extends Controller
{
    /**
     * @var JWTGuard
     */
    protected $guard;

    public function __construct()
    {
        $this->guard = auth();
    }

    /**
     * @param  RegistrationRequest  $request
     *
     * @return JsonResponse
     */
    public function register(RegistrationRequest $request): JsonResponse
    {
        $userData = $request->validated();

        $user = User::query()->create($userData);
        $token = $this->guard->login($user);

        return response()->json(new JWTAuthResource($this->guard->user(), (string)$token));
    }

    /**
     * @param  LoginRequest  $request
     *
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $credentials = $request->validated();

        if (!$token = $this->guard->attempt($credentials)) {
            return response()->json(['message' => 'Неверный логин или пароль'], 401);
        }

        return response()->json(new JWTAuthResource($this->guard->user(), $token));
    }

    /**
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        $this->guard->logout();
        return response()->json();
    }
}
