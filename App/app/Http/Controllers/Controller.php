<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Collection;
use Spatie\QueryBuilder\QueryBuilder;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var int
     */
    protected int $page;
    /**
     * @var int
     */
    protected int $perPage;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->page = abs((int)$request->page);
        $this->perPage = abs((int)$request->per_page);

        $this->perPage = $this->perPage ?: 10;
        $this->page = $this->page ?: 1;
    }

    /**
     * Пагинация списка.
     *
     * @param  Builder|Collection|QueryBuilder  $query  - выборка или коллекция объектов
     * @param  string|null  $resourceClass
     * @param  array  $additional
     *
     * @return array (Builder[]|float|int|mixed|object)[]
     */
    public function paginateQuerySet(Builder|Collection|QueryBuilder $query, string $resourceClass = null, $additional = []): array
    {
        $count = $query->count();
        $lastPage = (int)ceil($count / $this->perPage);
        if ($this->page > $lastPage) {
            $this->page = $lastPage;
        }
        $query = $query->skip(($this->page - 1) * $this->perPage)->take($this->perPage);
        $query = $query instanceof Collection ? $query : $query->get();

        /** @var JsonResource<class-string> $resourceClass */
        if ($resourceClass && get_parent_class($resourceClass) === JsonResource::class) {
            $query = $resourceClass::collection($query);
        }

        return [
            'items' => $query,
            'additional' => $additional,
            'last_page' => $lastPage,
            'total' => $count,
            'current_page' => $this->page,
            'per_page' => $this->perPage,
        ];
    }

    /**
     * @param  Builder|QueryBuilder  $query
     * @param  string|null  $resourceClass
     * @param  array  $additional
     *
     * @return array
     */
    public function cursorPaginatedQuerySet(Builder|QueryBuilder $query, string $resourceClass = null, array $additional = []): array
    {
        $paginator = $query->cursorPaginate($this->perPage);

        /** @var JsonResource<class-string> $resourceClass */
        if ($resourceClass && get_parent_class($resourceClass) === JsonResource::class) {
            $items = $resourceClass::collection($paginator->items());
        }

        return [
            'items' => $items ?? $paginator->items(),
            'additional' => $additional,
            'next_cursor' => $paginator->nextCursor()?->encode(),
            'total' => $query->count(),
            'previous_cursor' => $paginator->previousCursor()?->encode(),
            'per_page' => $paginator->perPage(),
        ];
    }
}
