<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewChatMessageEvent implements ShouldBroadcast
{
    use InteractsWithSockets;

    public function __construct(public $message, private readonly array $recipientIds, public int $chatId, public int $newMessageChats) {}

    public function broadcastOn()
    {
        $channels = [];
        foreach ($this->recipientIds as $recipientId) {
            $channels[] = new PrivateChannel(strtr('users.{user}.chats', ['{user}' => $recipientId]));
        }

        return $channels;
    }
}
