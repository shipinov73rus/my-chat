<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChatMessagesReadEvent implements ShouldBroadcast
{
    use InteractsWithSockets;

    public function __construct(public int $chatId, public int $recipientId) {}

    public function broadcastOn()
    {
        return new PrivateChannel(strtr('users.{user}.chats', ['{user}' => $this->recipientId]));
    }
}
