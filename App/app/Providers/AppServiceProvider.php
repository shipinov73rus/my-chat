<?php

namespace App\Providers;

use App\Contracts\ChatServiceContract;
use App\Contracts\UserServiceContract;
use App\Services\API\V1\ChatService;
use App\Services\API\V1\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(ChatServiceContract::class, ChatService::class);
        $this->app->bind(UserServiceContract::class, UserService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
