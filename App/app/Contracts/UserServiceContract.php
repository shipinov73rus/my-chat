<?php

namespace App\Contracts;

use App\Models\User;
use Spatie\QueryBuilder\QueryBuilder;

interface UserServiceContract
{
    /**
     * @return QueryBuilder
     */
    public function getUsersListBuilder(): QueryBuilder;

    /**
     * @return User
     */
    public function getMe(): User;
}
