<?php

namespace App\Contracts;

use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\ChatParticipant;
use Illuminate\Database\Eloquent\Builder;

interface ChatServiceContract
{
    /**
     * @return Builder
     */
    public function getDialoguesListBuilder(): Builder;

    /**
     * @param  array  $requestData
     *
     * @return ChatMessage
     */
    public function sendMessageAction(array $requestData): ChatMessage;

    /**
     * @param  Chat  $chat
     *
     * @return Builder
     */
    public function getChatMessagesBuilder(Chat $chat): Builder;

    /**
     * @param  Chat  $chat
     *
     * @return ChatParticipant
     */
    public function getChatRecipient(Chat $chat): ChatParticipant;

}
