<?php

namespace App\Services\API\V1;

use App\Contracts\ChatServiceContract;
use App\Events\ChatMessagesReadEvent;
use App\Events\NewChatMessageEvent;
use App\Http\Resources\Api\V1\MessageResource;
use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\ChatMessagesReading;
use App\Models\ChatParticipant;
use App\Models\Dialogue;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ChatService implements ChatServiceContract
{
    /**
     * @return Builder
     */
    public function getDialoguesListBuilder(): Builder
    {
        /** @var User $user */
        $user = auth()->user();

        return $user->chats()->with([
                'recipient.user',
                'latestMessage.author',
                'latestMessage' => fn ($q) => $q->withIsMessageRead()
            ])
            ->withCount('newMessages')
            ->orderByDesc(
                ChatMessage::query()->select('created_at')
                    ->whereColumn('chat_messages.chat_id', '=', 'chats.id')
                    ->orderByDesc('created_at')
                    ->limit(1)
            )
            ->getQuery();
    }

    /**
     * @param  array  $requestData
     *
     * @return ChatMessage
     */
    public function sendMessageAction(array $requestData): ChatMessage
    {
        /** @var User $user */
        $user = auth()->user();

        $usersIds = [$user->id, $requestData['recipient_id']];

        /** @var Dialogue $chat */
        $chat = Dialogue::query()
            ->whereHas('participants',
                fn (Builder $q) => $q->whereIn('participant_id', $usersIds), count: 2)
            ->firstOrCreate();

        if ($chat->wasRecentlyCreated) {
            $chat->participants()->createMany([
                ['participant_id' => $user->id],
                ['participant_id' => $requestData['recipient_id']]
            ]);
        }

        /** @var ChatMessage $message */
        $message = $chat->messages()->create([
            'message' => $requestData['message'],
            'chat_id' => $chat->id,
            'author_id' => $user->id
        ]);

        if ($chat->wasRecentlyCreated) {
           $message->readings()->create([
                'chat_id' => $chat->id,
                'user_id' => $usersIds[1],
                'max_read_msg_id' => 0
            ]);
        }

        $message->is_read = false;

        $newRecipientMessagesChats = User::query()->find($requestData['recipient_id'])->loadNewMessagesChatsCount()->chats_count;

        event(new NewChatMessageEvent(new MessageResource($message), $usersIds, $chat->id, $newRecipientMessagesChats));

        $this->readMyMessages($chat);

        return $message;
    }

    /**
     * @param  Chat  $chat
     *
     * @return Builder
     */
    public function getChatMessagesBuilder(Chat $chat): Builder
    {
        $this->readMyMessages($chat);

        return $chat->messages()
            ->with(['author'])
            ->withIsMessageRead()
            ->orderByDesc('created_at')
            ->getQuery();
    }

    /**
     * @param  Chat  $chat
     *
     * @return ChatParticipant
     */
    public function getChatRecipient(Chat $chat): ChatParticipant
    {
        /** @var ChatParticipant $recipient */
        $recipient = $chat->participants()
            ->with('user')
            ->where('participant_id', '!=', auth()->id())
            ->first();

        return $recipient;
    }

    /**
     * @param  Chat  $chat
     *
     * @return void
     */
    private function readMyMessages(Chat $chat): void
    {
        $maxReadMsgId = $chat->messages()->max('id');

        event(new ChatMessagesReadEvent($chat->id, $this->getChatRecipient($chat)->participant_id));

        ChatMessagesReading::query()
            ->updateOrCreate([
                'chat_id' => $chat->id,
                'user_id' => auth()->id()
            ], ['max_read_msg_id' => $maxReadMsgId]);
    }
}
