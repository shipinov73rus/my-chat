<?php

namespace App\Services\API\V1;

use App\Contracts\UserServiceContract;
use App\Http\Filters\UserSearchFilter;
use App\Models\User;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class UserService implements UserServiceContract
{
    /**
     * @return QueryBuilder
     */
    public function getUsersListBuilder(): QueryBuilder
    {
        return QueryBuilder::for(User::class)
            ->where('id', '!=', auth()->id())
            ->allowedFilters([
                AllowedFilter::custom('search', new UserSearchFilter())
            ]);
    }

    /**
     * @return User
     */
    public function getMe(): User
    {
        /** @var User $user */
        $user = auth()->user();

        return $user->loadNewMessagesChatsCount();
    }

}
