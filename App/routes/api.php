<?php

use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\ChatController;
use App\Http\Controllers\Api\V1\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('registration', [AuthController::class, 'register'])->name('sign-up');
Route::post('login', [AuthController::class, 'login'])->name('sign-in');

Route::middleware('auth:api')->group(function () {
    Route::get('me', [UserController::class, 'getMe'])->name('me');
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');
    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('{user}', [UserController::class, 'getUser'])->name('detail');
        Route::get('', [UserController::class, 'getUsersList'])->name('list');
    });

    Route::group(['prefix' => 'chats', 'as' => 'chats.'], function () {
        Route::get('', [ChatController::class, 'getChatsList'])->name('list');
        Route::get('{chat}', [ChatController::class, 'getChatMessages'])->name('messages');
        Route::post('send', [ChatController::class, 'sendMessage'])->name('send.message');
    });
});
